#ifndef MATRIX_SRC_S21_MATRIX_OOP_H_
#define MATRIX_SRC_S21_MATRIX_OOP_H_

#include <iostream>

class S21Matrix {
 private:
  // Attributes
  int rows_, cols_;
  double** matrix_;
  const float min_diff_ = 0.0000001;

  void CreateMatrix();
  void CopyMatrix(const S21Matrix& other);
  void RemoveMatrix(S21Matrix& other) noexcept;

 public:
  S21Matrix() noexcept;
  S21Matrix(long int rows, long int cols);
  S21Matrix(const S21Matrix& other);
  S21Matrix(S21Matrix&& other) noexcept;
  ~S21Matrix();

  int GetRows() const noexcept;
  int GetCols() const noexcept;
  void SetRows(int len_row);
  void SetCols(int len_col);

  bool EqualColRow(const S21Matrix& other) const noexcept;

  bool EqMatrix(const S21Matrix& other) const noexcept;
  void SumMatrix(const S21Matrix& other);
  void SubMatrix(const S21Matrix& other);
  void MulNumber(const double num) const noexcept;
  void MulMatrix(const S21Matrix& other);
  S21Matrix Transpose() const;
  S21Matrix CalcComplements() const;
  double Determinant() const;
  S21Matrix InverseMatrix() const;

  S21Matrix operator+(const S21Matrix& other) const;
  S21Matrix operator-(const S21Matrix& other) const;
  S21Matrix operator*(const double number) const noexcept;
  S21Matrix operator*(const S21Matrix& other) const;
  bool operator==(const S21Matrix& other) const noexcept;
  S21Matrix& operator=(S21Matrix& other);
  S21Matrix& operator=(S21Matrix&& other) noexcept;
  S21Matrix& operator+=(const S21Matrix& other);
  S21Matrix& operator-=(const S21Matrix& other);
  S21Matrix& operator*=(const S21Matrix& other);
  S21Matrix& operator*=(const double number) noexcept;
  double& operator()(const int i, const int j);
  const double& operator()(const int i, const int j) const;

  void InsertArray(double* dum);
  void PrintMatrix();
  S21Matrix GetCutMatrix(int row, int col) const;
};

S21Matrix operator*(const double number, const S21Matrix& other) noexcept;
#endif  // MATRIX_SRC_S21_MATRIX_OOP_H_