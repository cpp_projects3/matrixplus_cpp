#include "s21_matrix_oop.h"

/*--------------------------
    Conctructors
---------------------------*/
S21Matrix::S21Matrix() noexcept : rows_(0), cols_(0), matrix_(nullptr) {}
S21Matrix::S21Matrix(long int rows, long int cols) : rows_(rows), cols_(cols) {
  if (rows_ <= 0 || cols_ <= 0) {
    throw std::out_of_range("Matrix has no value or size less than 1X1");
  }
  CreateMatrix();
}
S21Matrix::S21Matrix(const S21Matrix& other)
    : rows_(other.rows_), cols_(other.cols_) {
  CopyMatrix(other);
}
S21Matrix::S21Matrix(S21Matrix&& other) noexcept
    : rows_(other.rows_), cols_(other.cols_), matrix_(other.matrix_) {
  other.rows_ = 0;
  other.cols_ = 0;
  other.matrix_ = nullptr;
}
S21Matrix::~S21Matrix() {
  if (matrix_) {
    RemoveMatrix(*this);
  }
}
/*--------------------------
    private functions
---------------------------*/
// Создание матрицы (Перед созданием должна быть задана rows_ и cols_)
void S21Matrix::CreateMatrix() {
  matrix_ = new double*[rows_]();
  for (int i = 0; i < rows_; i++) {
    try {
      matrix_[i] = new double[cols_]();
    } catch (...) {
      for (int j = 0; j < i; j++) delete[] matrix_[j];
      delete[] matrix_;
      throw;
    }
  }
}
// Освобождение памяти. Удаление матрицы
void S21Matrix::RemoveMatrix(S21Matrix& other) noexcept {
  for (int i = 0; i < other.rows_; i++) {
    delete[] other.matrix_[i];
  }
  delete[] other.matrix_;
}
void S21Matrix::CopyMatrix(const S21Matrix& other) {
  rows_ = other.rows_;
  cols_ = other.cols_;
  CreateMatrix();
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      matrix_[i][j] = other.matrix_[i][j];
    }
  }
}

/*--------------------------
    public main functions
---------------------------*/
bool S21Matrix::EqMatrix(const S21Matrix& other) const noexcept {
  if (!this->EqualColRow(other)) {
    return false;
  }
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      if (std::abs(matrix_[i][j] - other.matrix_[i][j]) >= min_diff_) {
        return false;
      }
    }
  }
  return true;
}

void S21Matrix::SumMatrix(const S21Matrix& other) {
  if (!this->EqualColRow(other)) {
    throw std::out_of_range("Error! Matrixes should have the same size");
  }
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      matrix_[i][j] = matrix_[i][j] + other.matrix_[i][j];
    }
  }
}

void S21Matrix::SubMatrix(const S21Matrix& other) {
  if (!EqualColRow(other)) {
    throw std::out_of_range("Error! Matrixes should have the same size");
  }
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      matrix_[i][j] = matrix_[i][j] - other.matrix_[i][j];
    }
  }
}

void S21Matrix::MulNumber(const double num) const noexcept {
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      matrix_[i][j] *= num;
    }
  }
}

bool S21Matrix::EqualColRow(const S21Matrix& other) const noexcept {
  return other.cols_ == cols_ && other.rows_ == rows_;
}

void S21Matrix::MulMatrix(const S21Matrix& other) {
  if (this->cols_ != other.rows_) {
    throw std::out_of_range(
        "Matrixes should have same size (matrix_1:col == matrix_2:rows_");
  }
  S21Matrix buff(this->rows_, other.cols_);
  for (int i = 0; i < this->rows_; i++) {
    for (int j = 0; j < other.cols_; j++) {
      for (int q = 0; q < other.rows_; q++) {
        buff.matrix_[i][j] += matrix_[i][q] * other.matrix_[q][j];
      }
    }
  }
  RemoveMatrix(*this);
  this->CopyMatrix(buff);
}

S21Matrix S21Matrix::Transpose() const {
  S21Matrix new_matrix(cols_, rows_);
  for (int i = 0; i < cols_; i++) {
    for (int j = 0; j < rows_; j++) {
      new_matrix.matrix_[i][j] = matrix_[j][i];
    }
  }
  return new_matrix;
}

S21Matrix S21Matrix::CalcComplements() const {
  if (this->rows_ != this->cols_) {
    throw std::out_of_range("Error! The matrix is not quadratic.");
  }
  S21Matrix result(rows_, cols_);
  int sign = 1;
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      double buff;
      S21Matrix temp = GetCutMatrix(i, j);
      buff = temp.Determinant();
      result.matrix_[i][j] = buff * sign;
      sign = sign * -1;
    }
    if (this->rows_ % 2 == 0) {
      sign = sign * -1;
    }
  }
  return result;
}

double S21Matrix::Determinant() const {
  if (cols_ != rows_) {
    throw std::out_of_range("Error! The matrix is not quadratic.");
  } else if (cols_ == 1) {
    return matrix_[0][0];
  } else {
    double result = 0;
    int sign = 1;
    for (int j = 0; j < cols_; j++) {
      double buff = 0;
      S21Matrix temp = GetCutMatrix(0, j);
      buff = temp.Determinant();
      buff = buff * matrix_[0][j] * sign;
      result += buff;
      sign = sign * -1;
    }
    return result;
  }
}

S21Matrix S21Matrix::InverseMatrix() const {
  double determ = this->Determinant();
  if (std::abs(determ) < min_diff_) {
    throw std::out_of_range("Determination = 0");
  }
  S21Matrix buff;
  if (rows_ == 1 || cols_ == 1) {
    buff.CopyMatrix(*this);
  } else {
    buff = Transpose();
    buff = buff.CalcComplements();
  }
  buff.MulNumber(1 / determ);
  return buff;
}

/*--------------------------
    public accessor and mutator functions
---------------------------*/
void S21Matrix::SetRows(int len_row) {
  if (len_row <= 0) {
    throw std::out_of_range("Error, size of len_row should be positive");
  }
  if (len_row != rows_) {
    double** buff_matrix = new double*[len_row];
    for (int i = 0; i < len_row; i++) {
      buff_matrix[i] = new double[cols_];
      for (int j = 0; j < cols_; j++) {
        if (i < rows_) {
          buff_matrix[i][j] = matrix_[i][j];
        } else {
          buff_matrix[i][j] = 0;
        }
      }
    }
    RemoveMatrix(*this);
    rows_ = len_row;
    matrix_ = buff_matrix;
  }
}

void S21Matrix::SetCols(int len_col) {
  if (len_col <= 0) {
    throw std::out_of_range("Error, size of len_col should be positive");
  }
  if (len_col != cols_) {
    double** buff_matrix = new double*[rows_];
    for (int i = 0; i < this->rows_; i++) {
      buff_matrix[i] = new double[len_col];
      for (int j = 0; j < len_col; j++) {
        if (j < cols_) {
          buff_matrix[i][j] = matrix_[i][j];
        } else {
          buff_matrix[i][j] = 0;
        }
      }
    }
    RemoveMatrix(*this);
    cols_ = len_col;
    matrix_ = buff_matrix;
  }
}

int S21Matrix::GetRows() const noexcept { return this->rows_; }

int S21Matrix::GetCols() const noexcept { return this->cols_; }

/*--------------------------
    public: перегруженные операторы
---------------------------*/
S21Matrix S21Matrix::operator+(const S21Matrix& other) const {
  S21Matrix result(*this);
  result.SumMatrix(other);
  return result;
}

S21Matrix S21Matrix::operator-(const S21Matrix& other) const {
  S21Matrix result(*this);
  result.SubMatrix(other);
  return result;
}

S21Matrix S21Matrix::operator*(const double number) const noexcept {
  S21Matrix result(*this);
  result.MulNumber(number);
  return result;
}

S21Matrix S21Matrix::operator*(const S21Matrix& other) const {
  S21Matrix result(*this);
  result.MulMatrix(other);
  return result;
}

S21Matrix operator*(const double number, const S21Matrix& other) noexcept {
  S21Matrix result(other);
  result.PrintMatrix();
  result.MulNumber(number);
  return result;
}

bool S21Matrix::operator==(const S21Matrix& other) const noexcept {
  return this->EqMatrix(other);
}

S21Matrix& S21Matrix::operator=(S21Matrix& other) {
  if (this != &other) {
    RemoveMatrix(*this);
    CopyMatrix(other);
  }
  return *this;
}

S21Matrix& S21Matrix::operator=(S21Matrix&& other) noexcept {
  if (this != &other) {
    RemoveMatrix(*this);
    rows_ = other.rows_;
    cols_ = other.cols_;
    matrix_ = other.matrix_;

    other.matrix_ = nullptr;
    other.rows_ = 0;
    other.cols_ = 0;
  }
  return *this;
}

S21Matrix& S21Matrix::operator+=(const S21Matrix& other) {
  this->SumMatrix(other);
  return *this;
}

S21Matrix& S21Matrix::operator-=(const S21Matrix& other) {
  this->SubMatrix(other);
  return *this;
}

S21Matrix& S21Matrix::operator*=(const S21Matrix& other) {
  this->MulMatrix(other);
  return *this;
}

S21Matrix& S21Matrix::operator*=(const double number) noexcept {
  this->MulNumber(number);
  return *this;
}

double& S21Matrix::operator()(const int i, const int j) {
  if (rows_ <= i || cols_ <= j) {
    throw std::out_of_range("Error! Index is out of range.");
  } else if (i < 0 || j < 0) {
    throw std::out_of_range("Error! Index is lower than 0.");
  }
  return matrix_[i][j];
}

const double& S21Matrix::operator()(const int i, const int j) const {
  if (rows_ <= i || cols_ <= j) {
    throw std::out_of_range("Error! Index is out of range.");
  } else if (i < 0 || j < 0) {
    throw std::out_of_range("Error! Index is lower than 0.");
  }
  return matrix_[i][j];
}

/*--------------------------
    public: additional
---------------------------*/
/**
 * @brief
 * Матрица предназначена для ввода новых данных. Заменяет старые данные.
 * @param dum Принимает одномерный массив
 */
void S21Matrix::InsertArray(double* dum) {
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      matrix_[i][j] = dum[i * cols_ + j];
    }
  }
}

/**
 * @brief
 * Распечатка данных матрицы в консоль.
 */
void S21Matrix::PrintMatrix() {
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      std::cout << matrix_[i][j] << " ";
    }
    std::cout << std::endl;
  }
}

/**
 * @brief Обрезка матрицы по строке и колонке. Сохранение в матрицу temp
 *
 * @param row номер строки, котору необходимо обрезать
 * @param col номер колонки, котору необходимо обрезать
 * @param temp сохраняем данные в новую матрицу
 */
S21Matrix S21Matrix::GetCutMatrix(int row, int col) const {
  int buff_row = 0;
  S21Matrix buff_matrix(rows_ - 1, cols_ - 1);
  for (int i = 0; i < rows_; i++) {
    int column = 0;
    if (i != row) {
      for (int j = 0; j < cols_; j++) {
        if (j != col) {
          buff_matrix.matrix_[buff_row][column++] = matrix_[i][j];
        }
      }
      buff_row++;
    }
  }
  return buff_matrix;
}
